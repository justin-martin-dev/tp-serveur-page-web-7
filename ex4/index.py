import cgi
form = cgi.FieldStorage()
print("Content-type: text/html; charset=utf-8\n")

authorizedUser = {
    "justin": "nice_password",
    "ph": "secure_password"
}

username=form.getvalue("name")
password=form.getvalue("password")

if authorizedUser.__contains__(username) and authorizedUser[username] == password:
    html = """<!DOCTYPE html>
    <head>
     <title>Authorisé</title>
    </head>
    <body>
        <h2>
            Votre page sécurisée """+form.getvalue("name")+"""
        </h2>
    </body>
    </html>
    """
else:
    html = """<!DOCTYPE html>
    <head>
     <title>Failed authentification</title>
    </head>
    <body>
        <h1 style="color: #c0392b">Failed to auth</h1>
    </body>
    </html>
    """


print(html)